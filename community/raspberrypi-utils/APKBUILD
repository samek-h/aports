# Contributor: macmpi <spam@ipik.org>
# Maintainer: macmpi <spam@ipik.org>
pkgname=raspberrypi-utils
pkgver=0.20230928
pkgrel=0
_commit="b2cc141e724522f10e57247925df896375aa0e28"
pkgdesc="Collection of Raspberry Pi utilities (scripts and simple applications)"
url="https://github.com/raspberrypi/utils"
arch="armhf armv7 aarch64"
license="BSD-3-Clause"
makedepends="cmake samurai dtc-dev"
source="$pkgname-$_commit.tar.gz::https://github.com/raspberrypi/utils/archive/$_commit.tar.gz"
builddir="$srcdir/utils-$_commit"
# does not have any tests
options="!check"
subpackages="
	$pkgname-vclog
	$pkgname-raspinfo::noarch
	$pkgname-dtmerge
	$pkgname-dtmerge-doc:dtmerge_doc:noarch
	$pkgname-ovmerge::noarch
	$pkgname-overlaycheck::noarch
	$pkgname-otpset::noarch
	$pkgname-pinctrl
	$pkgname-pinctrl-bash-completion:pinctrl_bashcomp:noarch
	"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_DATADIR=/usr/share \
		-DCMAKE_BUILD_TYPE=MinSizeRel
	cmake --build build
}

package() {
	depends="
		$pkgname-vclog=$pkgver-r$pkgrel
		$pkgname-raspinfo=$pkgver-r$pkgrel
		$pkgname-dtmerge=$pkgver-r$pkgrel
		$pkgname-ovmerge=$pkgver-r$pkgrel
		$pkgname-overlaycheck=$pkgver-r$pkgrel
		$pkgname-otpset=$pkgver-r$pkgrel
		$pkgname-pinctrl=$pkgver-r$pkgrel
		"
	DESTDIR="$pkgdir" cmake --install build
}

vclog() {
	pkgdesc="A tool to get VideoCore 'assert' or 'msg' logs with optional -f to wait for new logs to arrive."
	depends=""

	amove usr/bin/vclog
}

raspinfo() {
	pkgdesc="A short script to dump information about the Pi. Intended for the submission of bug reports."
	depends="
		$pkgname-vclog=$pkgver-r$pkgrel
		$pkgname-pinctrl=$pkgver-r$pkgrel
		bash
		raspberrypi-userland
		sudo-virt
		usbutils
		"
	# missing https://github.com/raspberrypi/rpi-eeprom

	amove usr/bin/raspinfo
}

dtmerge() {
	pkgdesc="A tool for applying compiled DT overlays (*.dtbo) to base Device Tree files (*.dtb)."
	depends=""

	amove usr/bin/dtmerge
}

dtmerge_doc() {
	pkgdesc="Documentation for $pkgname-dtmerge"
	depends="$pkgname-dtmerge=$pkgver-r$pkgrel"
	install_if="$pkgname-dtmerge=$pkgver-r$pkgrel docs"

	gzip -n -9 "$pkgdir"/usr/share/man/man1/dtmerge.1
	amove usr/share/man/man1/dtmerge.1.gz
}

ovmerge() {
	pkgdesc="A tool for merging DT overlay source files (*-overlay.dts), flattening and sorting .dts files for easy comparison, displaying the include tree, etc."
	depends="perl"

	amove usr/bin/ovmerge
}

overlaycheck() {
	pkgdesc="A tool for validating the overlay files and README in a kernel source tree."
	depends="
		$pkgname-dtmerge=$pkgver-r$pkgrel
		$pkgname-ovmerge=$pkgver-r$pkgrel
		dtc
		perl
		"

	amove usr/bin/overlaycheck
	amove usr/bin/overlaycheck_exclusions.txt
}

otpset() {
	pkgdesc="A short script to help with reading and setting the customer OTP bits."
	depends="python3 raspberrypi-userland"

	amove usr/bin/otpset
}

pinctrl() {
	pkgdesc="A tool for displaying and modifying the GPIO and pin muxing state of a system, bypassing the kernel."
	depends=""

	amove usr/bin/pinctrl
}

pinctrl_bashcomp() {
	depends="$pkgname-pinctrl=$pkgver-r$pkgrel"
	pkgdesc="Bash completions for $pkgname-pinctrl"
	install_if="$pkgname-pinctrl=$pkgver-r$pkgrel bash-completion"

	amove usr/share/bash-completion/completions/pinctrl
}

sha512sums="
20e412b844eab70cfaf11ddfc07f6c7e3537a6209c888c3c3fa16f26c65c1a9c0b4fe697c274d8cf32ba58ca63136423171d1b48392e3d7ec358c8d9eb2569cc  raspberrypi-utils-b2cc141e724522f10e57247925df896375aa0e28.tar.gz
"
