# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=konversation
pkgver=23.08.1
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://konversation.kde.org/"
pkgdesc="A user-friendly and fully-featured IRC client"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	karchive5-dev
	kbookmarks5-dev
	kconfig5-dev
	kconfigwidgets5-dev
	kcoreaddons5-dev
	kcrash5-dev
	kdbusaddons5-dev
	kdoctools5-dev
	kglobalaccel5-dev
	ki18n5-dev
	kiconthemes5-dev
	kidletime5-dev
	kio5-dev
	kitemviews5-dev
	knewstuff5-dev
	knotifications5-dev
	knotifyconfig5-dev
	kparts5-dev
	kwallet5-dev
	kwidgetsaddons5-dev
	kwindowsystem5-dev
	phonon-dev
	qca-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	samurai
	solid5-dev
	"
_repo_url="https://invent.kde.org/network/konversation.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/konversation-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ce739668c6d65dad89be85aa126c2e5e8e02cd18b3864cdce63415e94704d1ed301ec62ec8e3346ebb5516f43a4f5801600dd163357960599275ac7aed42d3d9  konversation-23.08.1.tar.xz
"
