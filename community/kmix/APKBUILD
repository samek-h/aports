# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kmix
pkgver=23.08.1
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/multimedia/org.kde.kmix"
pkgdesc="A sound channel mixer and volume control"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	alsa-lib-dev
	extra-cmake-modules
	kcompletion5-dev
	kconfig5-dev
	kconfigwidgets5-dev
	kcrash5-dev
	kdbusaddons5-dev
	kdoctools5-dev
	kglobalaccel5-dev
	ki18n5-dev
	kiconthemes5-dev
	knotifications5-dev
	kwidgetsaddons5-dev
	kwindowsystem5-dev
	kxmlgui5-dev
	libcanberra-dev
	plasma-framework5-dev
	pulseaudio-dev
	qt5-qtbase-dev
	samurai
	solid5-dev
	"
_repo_url="https://invent.kde.org/multimedia/kmix.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmix-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
efd66b0709b26f74ad702e249d1a43da80262d3d1a80d58346eaaa3d5c7e2f988d70858aa6d030956cf3bb424a8f8a9f9fdf8a16bcf83a44b9aafc6dcdfd49ed  kmix-23.08.1.tar.xz
"
