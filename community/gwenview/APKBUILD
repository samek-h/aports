# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=gwenview
pkgver=23.08.1
pkgrel=2
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> purpose
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/graphics/org.kde.gwenview"
pkgdesc="Fast and easy to use image viewer by KDE"
license="GPL-2.0-only"
depends="
	kimageformats5
	qt5-qtimageformats
	"
makedepends="
	baloo5-dev
	extra-cmake-modules
	kactivities5-dev
	kdoctools5-dev
	ki18n5-dev
	kiconthemes5-dev
	kimageannotator-dev
	kio5-dev
	kitemmodels5-dev
	knotifications5-dev
	kparts5-dev
	kwindowsystem5-dev
	lcms2-dev
	libjpeg-turbo-dev
	libkdcraw-dev
	libpng-dev
	purpose5-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtx11extras-dev
	samurai
	"
checkdepends="
	dbus
	kinit5
	xvfb-run
	"
_repo_url="https://invent.kde.org/graphics/gwenview.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/gwenview-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# urlutilstest and placetreemodeltest are broken
	# recursivedirmodeltest and contextmanagertest requires running DBus
	local skipped_tests="("
	local tests="
		contextmanager
		documenttest
		jpegcontenttest
		placetreemodel
		recursivedirmodel
		urlutils
		"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	CTEST_OUTPUT_ON_FAILURE=TRUE dbus-run-session xvfb-run ctest -E "$skipped_tests"

}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3059b92ce95e3165d91182cb45255e794fbdd3b5bed84fd13cc9ded9c0ad07044bac23368471fcb0c96b041e25b95f9add46194cb3a5ca981bc3c6727fc89199  gwenview-23.08.1.tar.xz
"
