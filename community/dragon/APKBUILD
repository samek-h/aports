# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=dragon
pkgver=23.08.1
pkgrel=1
pkgdesc="A multimedia player where the focus is on simplicity, instead of features"
url="https://kde.org/applications/multimedia/org.kde.dragonplayer"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-only OR GPL-3.0-only"
depends="phonon-backend-gstreamer"
makedepends="
	extra-cmake-modules
	kconfig5-dev
	kconfigwidgets5-dev
	kcoreaddons5-dev
	kcrash5-dev
	kdbusaddons5-dev
	kdoctools5-dev
	ki18n5-dev
	kio5-dev
	kjobwidgets5-dev
	knotifications5-dev
	kparts5-dev
	kwidgetsaddons5-dev
	kwindowsystem5-dev
	kxmlgui5-dev
	phonon-dev
	qt5-qtbase-dev
	samurai
	solid5-dev
	"
_repo_url="https://invent.kde.org/multimedia/dragon.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/dragon-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
36cd1a81de65cbe77dd8610216d0e38902dc77bbad66b33f4950ba0303a078767778d9fb5862e9060a508073195a2c1d3cb835bcfde304d62053fd736c2d7b43  dragon-23.08.1.tar.xz
"
