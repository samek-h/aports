# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=akonadiconsole
pkgver=23.08.1
pkgrel=1
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by akonadi
# ppc64le blocked by calendarsupport
arch="all !armhf !s390x !ppc64le !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Application for debugging Akonadi Resources"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-contacts-dev
	akonadi-dev
	akonadi-search-dev
	calendarsupport-dev
	extra-cmake-modules
	kcalendarcore5-dev
	kcompletion5-dev
	kcompletion5-dev
	kconfig5-dev
	kconfigwidgets5-dev
	kcontacts5-dev
	kcrash5-dev
	kdbusaddons5-dev
	kdoctools5-dev
	ki18n5-dev
	kio5-dev
	kitemmodels5-dev
	kitemviews5-dev
	kmime-dev
	ktextwidgets5-dev
	kwidgetsaddons5-dev
	kxmlgui5-dev
	libkdepim-dev
	messagelib-dev
	qt5-qtbase-dev
	samurai
	xapian-bindings
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/akonadiconsole.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadiconsole-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
178d990488929dc610452a17671b0e9f04382b61445429e65721a1b3dacfe911fe522b786a0c99d0cfe17682f1d7b2f59621eec71c8711d33c0fa394535d1a14  akonadiconsole-23.08.1.tar.xz
"
