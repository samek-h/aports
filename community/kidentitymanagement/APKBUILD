# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kidentitymanagement
pkgver=23.08.1
pkgrel=1
pkgdesc="KDE PIM libraries"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org"
license="LGPL-2.0-or-later"
depends_dev="
	kcodecs5-dev
	kcompletion5-dev
	kconfig5-dev
	kcoreaddons5-dev
	kiconthemes5-dev
	kio5-dev
	kpimtextedit-dev
	ktextwidgets5-dev
	kxmlgui5-dev
	qt5-qtbase-dev
	"
makedepends="
	$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/kidentitymanagement.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kidentitymanagement-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kpimidentity-signaturetest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kpimidentity-signaturetest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ba632e84dbeca593e8cfe29c9786007042c45ee249bd06368cfa66fa55382ecb1891c047ce36d2e32441809f1476ef62e562d7a597a985c027adef3ec77c2d8d  kidentitymanagement-23.08.1.tar.xz
"
