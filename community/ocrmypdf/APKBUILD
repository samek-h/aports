# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Maintainer: Carlo Landmeter <clandmeter@alpinelinux.org>
pkgname=ocrmypdf
pkgver=15.1.0
pkgrel=0
pkgdesc="Add OCR text layer to scanned PDF files"
url="https://github.com/ocrmypdf/OCRmyPDF"
# s390x, armhf, x86, ppc64le: tesseract-ocr
arch="noarch !s390x !armhf !x86 !ppc64le"
license="MIT"
depends="
	ghostscript
	jbig2enc
	leptonica
	pngquant
	py3-deprecation
	py3-img2pdf
	py3-packaging
	py3-pdfminer
	py3-pikepdf
	py3-pillow
	py3-pluggy
	py3-reportlab
	py3-rich
	python3
	qpdf
	tesseract-ocr
	unpaper
	"
makedepends="
	py3-gpep517
	py3-setuptools_scm
	py3-wheel
	"
checkdepends="
	py3-hypothesis
	py3-pytest
	py3-pytest-cov
	py3-pytest-xdist
	tesseract-ocr-data-eng
	tesseract-ocr-data-osd
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/o/ocrmypdf/ocrmypdf-$pkgver.tar.gz"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	PYTHONPATH=src \
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/ocrmypdf*.whl
}

sha512sums="
fbafe8f40c6997055a7c560e6b09af99650c5af380fffb37d9d7174563dbac969ead6a9b7a1e519cd90042f352a12bcb68953723fc57ac2e413238f2b4a0e110  ocrmypdf-15.1.0.tar.gz
"
