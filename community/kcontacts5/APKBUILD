# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kcontacts5
pkgver=5.110.0
pkgrel=1
pkgdesc="Address book API for KDE"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later"
# TODO: Replace gnupg with specific gnupg subpackages that kcontacts really needs.
depends="
	gnupg
	iso-codes
	"
depends_dev="
	kcodecs5-dev
	kconfig5-dev
	kcoreaddons5-dev
	ki18n5-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/kcontacts.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcontacts-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
builddir="$srcdir/kcontacts-$pkgver"

replaces="kcontacts<=5.110.0-r0"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# kcontacts-addresstest requires Wayland display
	xvfb-run ctest --test-dir build --output-on-failure -E "kcontacts-addresstest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d365ae94a28f025094b7489ed2188134221ab70f2d0c220877db49da94cf325d51f5c83c117b0a45ac4f5179648cd862c3983716a599bd2435c8f3148c470fa4  kcontacts-5.110.0.tar.xz
"
