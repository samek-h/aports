# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=pyradio
pkgver=0.9.2.15
pkgrel=0
pkgdesc="Curses based internet radio player"
url="https://www.coderholic.com/pyradio"
arch="noarch"
license="MIT"
depends="
	python3
	py3-dateutil
	py3-dnspython
	py3-netifaces
	py3-psutil
	py3-requests
	py3-rich
	"
makedepends="py3-gpep517 py3-installer py3-setuptools py3-wheel"
options="!check" # no testsuite
subpackages="$pkgname-doc $pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/coderholic/pyradio/archive/refs/tags/$pkgver.tar.gz"

prepare() {
	default_prepare

	sed -i 's/^distro = None$/distro = AlpineLinux/' pyradio/config
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl

	install -Dm644 devel/pyradio.png -t "$pkgdir"/usr/share/icons/hicolor/512x512/apps/
	install -Dm644 devel/pyradio.desktop -t "$pkgdir"/usr/share/applications/

	install -Dm644 pyradio.1 pyradio_rb.1 pyradio_server.1 \
		-t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
2637b755033e74d0f552f3f221704996073d2e4ebd9474bbfff208685188a2e904605cf135af15171086e3e48bbf87885b94959aa4d111db059202b1d624195a  pyradio-0.9.2.15.tar.gz
"
